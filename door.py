#!/usr/bin/env python
# -*- coding: utf-8 -*-
import RPi.GPIO as GPIO    #Importamos la librería GPIO
from time import sleep                #Importamos time (time.sleep)
DOOR_OPEN=1
DOOR_CLOSE=0

GPIO.setmode(GPIO.BCM)     #Ponemos la placa en modo BCM
GPIO_DOOR = 14            #Usamos el pin GPIO 14 como TRIGGER

GPIO.setup(GPIO_DOOR,GPIO.IN,GPIO.PUD_UP) 

def readState():
   value= GPIO.input(GPIO_DOOR)
   if value==1:
	return DOOR_CLOSE
   else:
      return DOOR_OPEN
  
def main():
   try:
      while (True):
       print readState()
       print "\n"	
       sleep(0.2)	

   except KeyboardInterrupt:
      print "quit"
   GPIO.cleanup()
   

if __name__=="__main__":
   main()

