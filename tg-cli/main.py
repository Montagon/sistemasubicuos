#!usr/bin/env
# -*- coding: utf-8 -*-
import pexpect
import time
 
"""
	CONFIGURACION
"""

grupo = "prueba"
usuario = "José Carlos Montañez"


telegram = pexpect.spawn('bin/telegram-cli -k tg.pub') #Inicia Telegram
telegram.expect("]")
print telegram.before

while True:
	#telegram.expect(" >>> ", timeout=None)
	print "Esperando..."
	a = telegram.after
	print a
	telegram.expect("\[.*\] .*(prueba).* .*(Javier Rodriguez Vazquez|José Carlos Montañez).* (>>>|»»») ", timeout=None)
	a = telegram.after
	print telegram.before
	print "\n----\n"
	print telegram.after

	"""
		FILTRAMOS Y COGEMOS EL GRUPO
	"""
	aux = a.split("[35;1m")
	aux = aux[2].split(" ")
	grupo_cad = aux[0]
	# FIN GRUPO

	"""
		NOMBRE
	"""
	b = a.split("[1;31m")
	b = b[1]
	b = b.split("\x1b[") 
	usuario_cad = b[0]
	# FIN USUARIO
	
	"""
		MENSAJE
	"""
	telegram.expect("\r\n")
	mensaje = telegram.before
	# FIN MENSAJE

	if(mensaje.find("salir") != -1):
		exit()
	elif(mensaje.find("dice") != -1):
		telegram.sendline("msg " + grupo + " qué digo")

	#telegram.sendline("msg prueba esto es un mensaje")
# print telegram.before
# telegram.sendline('chat_with_peer Javier_Rodriguez_Vazquez')
# telegram.interact()
# telegram.expect('\r\n>')     
# time.sleep(2)                      #Espera a que termine de iniciar
# telegram.sendline('msg '+contacto+' '+mensaje)   #Ejecuta el comando msg
# print ('Mensaje enviado a '+ contacto)           #Notifica que ya se ha mandado el mensaje
# telegram.sendline('quit')                        #cierra Telegram
# telegram.close()
