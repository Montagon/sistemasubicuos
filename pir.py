#!/usr/bin/env python
# -*- coding: utf-8 -*-
import RPi.GPIO as GPIO    #Importamos la librería GPIO
import time                #Importamos time (time.sleep)

GPIO.setmode(GPIO.BCM)     #Ponemos la placa en modo BCM
GPIO_PIR = 22             #Usamos el pin GPIO 25 como TRIGGER
GPIO.setup(GPIO_PIR,GPIO.IN)  #Configuramos Trigger como salida

MOTION_ON=1
MOTION_OFF=0 

def movement():
	if GPIO.input(GPIO_PIR):
           return MOTION_ON	
	else:
           return MOTION_OFF
	   
def main():
   print movement()

if __name__=="__main__":
   main()
   GPIO.cleanup()

