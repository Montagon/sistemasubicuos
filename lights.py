#!/usr/bin/env python
# -*- coding: utf-8 -*-
import RPi.GPIO as GPIO    #Importamos la librería GPIO
from time import sleep                #Importamos time (time.sleep)
LIGHT_ON=1
LIGHT_OFF=0

GPIO.setmode(GPIO.BCM)     #Ponemos la placa en modo BCM
GPIO_LIGHT = 24            #Usamos el pin GPIO 25 como TRIGGER

GPIO.setup(GPIO_LIGHT,GPIO.OUT) #Configuramos Trigger como salida
GPIO.output(GPIO_LIGHT, False)  #Ponemos el Pin a 0 

def switch(state):
   GPIO.output(GPIO_LIGHT,state)
      
def main():
   switch(LIGHT_ON)
   sleep(2)
   switch(LIGHT_OFF)
   sleep(2)
   switch(LIGHT_ON)
   sleep(2)
   switch(LIGHT_OFF)

if __name__=="__main__":
   main()
   GPIO.cleanup()

