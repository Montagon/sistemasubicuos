#!/usr/bin/env python
# -*- coding: utf-8 -*-
import RPi.GPIO as GPIO    #Importamos la librería GPIO
import time                #Importamos time (time.sleep)
GPIO.setmode(GPIO.BCM)     #Ponemos la placa en modo BCM
GPIO_TRIGGER = 7          #Usamos el pin GPIO 7 como TRIGGER
GPIO_ECHO    = 8           #Usamos el pin GPIO 8 como ECHO
GPIO.setup(GPIO_TRIGGER,GPIO.OUT)  #Configuramos Trigger como salida
GPIO.setup(GPIO_ECHO,GPIO.IN)      #Configuramos Echo como entrada
GPIO.output(GPIO_TRIGGER,False)    #Ponemos el pin 7 como LOW
 
def outsideDistance(): 
   GPIO.output(GPIO_TRIGGER,True)   #Enviamos un pulso de ultrasonidos
   time.sleep(0.00001)              #Una pequeñña pausa
   GPIO.output(GPIO_TRIGGER,False)  #Apagamos el pulso
   start = time.time()              #Guarda el tiempo actual mediante time.time()
   while GPIO.input(GPIO_ECHO)==0:  #Mientras el sensor no reciba señal...
      start = time.time()          #Mantenemos el tiempo actual mediante time.time()
   while GPIO.input(GPIO_ECHO)==1:  #Si el sensor recibe señal...
      stop = time.time()           #Guarda el tiempo actual mediante time.time() en otra variable
   elapsed = stop-start             #Obtenemos el tiempo transcurrido entre envío y recepción
   distance = (elapsed * 34300)/2   #Distancia es igual a tiempo por velocidad partido por 2   D = (T x V)/2
   return distance			                 #Devolvemos la distancia (en centímetros) por pantalla

def main():
   print outsideDistance()

if __name__=="__main__":
   main()
   GPIO.cleanup()
