#!usr/bin/env
# -*- coding: utf-8 -*-
import pexpect
import time

class Domotic(object):
	"""docstring for Domotica"""
	def start_telegram(self):
		telegram = pexpect.spawn('tg-cli/bin/telegram-cli -k tg-cli/tg-server.pub')
		telegram.expect("]")
		return telegram
	def close_telegram(self):
		self.tgsendline('quit')
		self.tg.close()
		return 1
	def read_message(self, group):
		found = False
		while not found:
			self.tg.expect("\n", timeout=None)
			text = self.tg.before
			if text.find(group) != -1:
				#print "Find!"
				found = True
				"""
				USER
				"""
				if text.find("[1;31m") != -1:
					remaining_text = text.split("[1;31m")
					remaining_text = remaining_text[1]
					remaining_text = remaining_text.split("\x1b[") 
					user = remaining_text[0]

					"""
					MESSAGE
					"""
					if text.find(">>>") != -1:
						final_part = text.split(">>>")
						message = final_part[1].replace("\x1b[0m\r", "")
					elif text.find("»»»") != -1:
						final_part = text.split("»»»")
						message = final_part[1].replace("\x1b[0m\r", "")
					else:
						found = False
				else:
					found = False
		return user, message
	def send_message(self, to, msg):
		self.tg.sendline("msg " + to + " " + msg)
	def send_image(self, to, dir_img):
		self.tg.sendline("send_photo " + to + " " + dir_img)
	def __init__(self):
		super(Domotica, self).__init__()
		self.tg = self.start_telegram()