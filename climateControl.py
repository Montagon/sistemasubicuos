#!/usr/bin/env python
# -*- coding: utf-8 -*-

import RPi.GPIO as GPIO # Cargamos la libreria RPi.GPIO  
from time import sleep  # cargamos la función sleep del módulo time 
  
GPIO.setmode(GPIO.BCM)  # Ponemos la Raspberry en modo BCM  
MAX_TEMPERATURE=40      # Temperatura máxima para el climatizador
PINLED_CLIMATECONTROL=25   #PIN BCM para activar el LED indicador del número de grados

GPIO.setup(PINLED_CLIMATECONTROL, GPIO.OUT)  # Ponemos el pin GPIO nº25 como salida para el LED   
led = GPIO.PWM(PINLED_CLIMATECONTROL, 100)   # Creamos el objeto 'led' sobre el pin "LED_CLIMATECONTROL" a 100 Hz 
led.start(0)                                 # Iniciamos el objeto 'led' a 0% del ciclo de trabajo (completamente apagado)  

def climateControl(n_degrees):
   if (n_degrees<0):
	n_degrees=0
   elif (n_degrees>MAX_TEMPERATURE):
	n_degrees=40
	
   led.ChangeDutyCycle(n_degrees*100/MAX_TEMPERATURE) #Mapeamos el 100% a 40 grados Celcius
   sleep(2)

def main():
   climateControl(5) # Ponemos el climatizador a 5 grados y el led a una determinada tensión
   climateControl(20) # Ponemos el climatizador a 20 grados y el led a una determinada tensión
   climateControl(40) # Ponemos el climatizador a 40 grados y el led a una determinada tensión

   GPIO.cleanup()

if __name__== "__main__":
   main()

