#!/usr/bin/env python
# -*- coding: utf-8 -*-
import threading

LIGHT_ON=1
LIGHT_OFF=0
MAX_DISTANCE=500
MOTION_ON=1
MOTION_OFF=0
DOOR_OPEN=1
DOOR_CLOSE=0

from climateControl import climateControl
from door import readState
from lights import switch
from ultrasonic import outsideDistance
from pir import movement


class Sensors:
   
    """Constructor"""
    def __init__(self):
		self.lock = threading.Lock() # Lock to access into the monitor
		self.climateControl=LIGHT_OFF
		self.light=LIGHT_OFF

    def get_climateControl(self):
		return self.climateControl
		
    def get_light(self):
		return self.light
	
    def get_distance(self):
		self.lock.acquire() 
		distance=self.__read_distance()
		self.lock.release()
		return distance
	
    def get_presence(self):
		self.lock.acquire()
		presence= self.__read_presence()
		self.lock.release()
		return presence
		
    def get_door(self):
		self.lock.acquire()
		door= self.__read_door()
		self.lock.release()
		return door
	
    def set_climateControl(self,degrees):
		self.lock.acquire() 
		self.climateControl=degrees # Update the climateControl attrib
		self.__write_climateControl(degrees) # write the new state in Raspi 		
		self.lock.release()
		
    def set_light(self, state):
		self.lock.acquire()
		self.light=state # Update the light attrib
		self.__write_light(state) #write the new state in Raspi
		self.lock.release()

    def __read_distance(self):
		return outsideDistance()    
    
    def __read_presence(self):
	   return movement()
   
    def __read_door(self):
		return readState()
		
    def __write_climateControl(self,degrees):
       climateControl(degrees)
       print "se escribe el climate Control en la Raspi",degrees
	   
    def __write_light(self,state):
	   switch(state)
	   print "se escribe el light en la Raspi", state
	   

def main():	   
	sensor=Sensors()	
	print "--------------------------- CLIMATE CONTROL ----------------------------------"
	print "Valor actual de climatizacion: ",sensor.get_climateControl()
	print "Estableciendo el valor de climatizacion a 10..."
	sensor.set_climateControl(10)
	print "Valor actual de climatizacion: ",sensor.get_climateControl()

	print "---------------------------- LIGHT CONTROL -----------------------------"
	sensor.set_light(LIGHT_OFF)
	print "Valor actual de luces: ",sensor.get_light()
	print "Estableciendo el valor de luces a ",LIGHT_ON
	sensor.set_light(LIGHT_ON)
	print "Valor actual de luces: ",sensor.get_light()

	print "--------------------------- DOOR STATE ------------------------------------"
	print "Valor actual de la puerta: ",sensor.get_door(),"\n"

	print "--------------------------- DISTANCE STATE --------------------------------"
	print "El valor de la distacia es: ",sensor.get_distance(),"\n"

	print "--------------------------- PRESENCE STATE --------------------------------"
	print "El valor de la presencia es: ",sensor.get_presence(),"\n"

if __name__ == "__main__":
    main()



		
		
