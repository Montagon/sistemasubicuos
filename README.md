#Sistemas Ubicuos 2015-2016
##Domótica mediante Telegram



###Instalación

Para la correcta instalación, hay que descargar el proyecto que está alojado en Bitbucket.org. Se descarga mediante el siguiente comando:

	git clone https://bitbucket.org/Montagon/sistemasubicuos.git

Una vez hecho, quizá haga falta compilar el cliente de consola de Telegram.

Paquetes extras necesarios:

	sudo apt-get install libreadline-dev libconfig-dev libssl-dev lua5.2 liblua5.2-dev libevent-dev libjansson-dev libpython-dev make 

Para ello nos movemos a la carpeta *tg-cli* y ejecutamos:

	./configure
	make
